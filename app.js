import { Product } from "./product.js";
import { Movie } from "./movie.js";
import { Album } from "./album.js";

var product1=new Product("Car",1000000,10);
console.log(product1.sellCopies());

var album1=new Album("John");
console.log(album1.sellCopies());

var movie=new Movie("qfsquytwsqi");
console.log(movie.sellCopies());

console.log(product1 instanceof Product);
console.log(product1 instanceof Album);
console.log(product1 instanceof Movie);

console.log(album1 instanceof Product);
console.log(album1 instanceof Album);
console.log(album1 instanceof Movie);

console.log(movie instanceof Product);
console.log(movie instanceof Album);
console.log(movie instanceof Movie);



