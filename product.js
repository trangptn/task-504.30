class Product {
    __name;
    __price;
    __numberOfcopies;

    constructor(paramName,paramPrice,paramNumber)
    {
        this.__name=paramName;
        this.__price=paramPrice;
        this.__numberOfcopies=paramNumber;
    }

    sellCopies()
    {
        return this.__name+" - "+this.__price+" - "+this.__numberOfcopies;
    }

    oderCopies(num){
        this.__numberOfcopies=num;
    }
}
export  {Product};

